<?php

namespace App\Http\Controllers;



use App\Tour;
use Illuminate\Http\Request;

class TestController extends Controller
{
    public  function Create(){

        return view('create');
    }


    public function Store(Request $request){

        $tours=new Tour();
        $tours->name=$request->name;
        $tours->family=$request->family;
        $tours->email=$request->email;
        $tours->save();

        return redirect()->back();


    }
}
